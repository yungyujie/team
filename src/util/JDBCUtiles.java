package util;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class JDBCUtiles {
	//创建c3p0连接池对象
	private static final ComboPooledDataSource dataSource = 
			new ComboPooledDataSource();
	//获得数据源
	public static DataSource getDataSource() {
		return dataSource;
	}
	
	//获得QueryRunner对象
	public static QueryRunner getQueryRunner() {
		return new QueryRunner(dataSource);
	}
	
	@Test
	public void testDataSource() throws SQLException {
		System.out.println(dataSource.getConnection().toString());
	}
	
}
